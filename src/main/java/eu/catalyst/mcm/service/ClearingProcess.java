package eu.catalyst.mcm.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mcm.payload.MarketAction;
import eu.catalyst.mcm.payload.MarketActionCounterOffer;
import eu.catalyst.mcm.payload.PayloadMEMO2MCMNotFlex;
import eu.catalyst.mcm.payload.PrioritisedAction;

@Stateless
@Path("/clearingProcess/")
public class ClearingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingProcess.class);

	public ClearingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public PayloadMEMO2MCMNotFlex clearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("-> clearingProcess");

		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		returnPayloadMEMO2MCMNotFlex
				.setInformationBrokerServerUrl(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl());
		returnPayloadMEMO2MCMNotFlex.setTokenMEMO(inputPayloadMEMO2MCMNotFlex.getTokenMEMO());
		returnPayloadMEMO2MCMNotFlex.setTokenMCM(inputPayloadMEMO2MCMNotFlex.getTokenMCM());
		returnPayloadMEMO2MCMNotFlex
				.setMarketClearingManagerUrl(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl());
		returnPayloadMEMO2MCMNotFlex.setInformationBrokerId(inputPayloadMEMO2MCMNotFlex.getInformationBrokerId());

		// ---------------------------------------------------------------------------
		// Clean CounterOffers from Previous Loop
		cleanPreviousCounterOffers(inputPayloadMEMO2MCMNotFlex);
		// --------------------------------------------------------------------------

		ArrayList<PrioritisedAction> returnPrioritisedActionList = new ArrayList<PrioritisedAction>();

		ArrayList<PrioritisedAction> notValidActionList = new ArrayList<PrioritisedAction>();
		ArrayList<PrioritisedAction> validActionList = new ArrayList<PrioritisedAction>();

		for (Iterator<PrioritisedAction> iterator = inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList()
				.iterator(); iterator.hasNext();) {

			PrioritisedAction myPrioritisedAction = iterator.next();
			log.debug("StatusId: " + myPrioritisedAction.getMarketaction().getStatusid());
			if (myPrioritisedAction.getMarketaction().getStatusid() == 2) {
				// 2== "valid"
				validActionList.add(myPrioritisedAction);
			} else {
				notValidActionList.add(myPrioritisedAction);
			}

		}

		try {
			URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
					+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId().intValue() + "/form/"
					+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getFormid().getForm() + "/marketsessions/"
					+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue() + "/actions/prioritised/");

			log.debug("Rest url: " + url.toString());

			HttpURLConnection connService = (HttpURLConnection) url.openConnection();
			connService.setDoOutput(true);
			connService.setRequestMethod("POST");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
			GsonBuilder gb = new GsonBuilder();

			// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();

			String record = gson.toJson(validActionList);

			System.out.println("Rest Request: " + record);

			OutputStream os = connService.getOutputStream();
			os.write(record.getBytes());

			int myResponseCode = connService.getResponseCode();
			log.debug("HTTP error code :" + myResponseCode);

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<ArrayList<PrioritisedAction>>() {
			}.getType();
			gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			gson = gb.create();
			returnPrioritisedActionList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(returnPrioritisedActionList));
			os.flush();
			os.close();
			br.close();
			connService.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Forzatura
		returnPrioritisedActionList = validActionList;

		ArrayList<PrioritisedAction> PrioritisedActionToUpdateList = new ArrayList<PrioritisedAction>();
		ArrayList<MarketActionCounterOffer> marketActionCounterOffersList = new ArrayList<MarketActionCounterOffer>();
		ArrayList<MarketActionCounterOffer> marketActionCounterOfferToDeleteList = new ArrayList<MarketActionCounterOffer>();

		// get list of MarketActionCounterOffers from IB

		try {

			URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketplace/"
					+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId().intValue()
					+ "/marketsessions/" + inputPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue()
					+ "/counteroffers/");

			log.debug("Start - Rest url: " + url.toString());
			HttpURLConnection connService = (HttpURLConnection) url.openConnection();

			connService.setDoOutput(true);
			connService.setRequestMethod("GET");
			connService.setRequestProperty("Content-Type", "application/json");
			connService.setRequestProperty("Accept", "application/json");
			connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());

			if (connService.getResponseCode() != HttpURLConnection.HTTP_OK
					&& connService.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT) {
				throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
			Type listType = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
			}.getType();
			GsonBuilder gb = new GsonBuilder();
			gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			Gson gson = gb.create();
			marketActionCounterOffersList = gson.fromJson(br, listType);
			log.debug("payload received: " + gson.toJson(marketActionCounterOffersList));
			br.close();
			connService.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// for each MarketActionCounterOffer:
		// get action bid
		// get action offer
		// perform clearing algorithm to calculate clearing price value for current
		// bid/offer couple
		// update the current bid and offers with the correct status and add in a list
		// to update
		// save temporally clearing price value to exchangedValue field of current
		// MarketActionCounterOffers

		for (Iterator<MarketActionCounterOffer> iterator = marketActionCounterOffersList.iterator(); iterator
				.hasNext();) {

			MarketActionCounterOffer marketActionCounterOffer = iterator.next();

			PrioritisedAction myMarketActionOffer = null;
			PrioritisedAction myMarketActionBid = null;
//				myMarketActionOffer = new Utilities()
//						.invokeGetMarketAction(marketActionCounterOffer.getMarketAction_Offer_id());

//				myMarketActionBid = new Utilities()
//						.invokeGetMarketAction(marketActionCounterOffer.getMarketAction_Bid_id());

			for (Iterator<PrioritisedAction> iterator2 = returnPrioritisedActionList.iterator(); iterator2.hasNext();) {
				PrioritisedAction myPrioritisedAction = iterator2.next();
				if (myPrioritisedAction.getMarketaction().getId().intValue() == marketActionCounterOffer
						.getMarketAction_Offer_id().intValue()) {
					myMarketActionOffer = myPrioritisedAction;

				}
				if (myPrioritisedAction.getMarketaction().getId().intValue() == marketActionCounterOffer
						.getMarketAction_Bid_id().intValue()) {
					myMarketActionBid = myPrioritisedAction;

				}

			}

			double priceOffer = myMarketActionOffer.getMarketaction().getPrice().doubleValue();
			double priceBid = myMarketActionBid.getMarketaction().getPrice().doubleValue();
//						if (priceBid < priceOffer) { //to reject
			if (priceBid > priceOffer) { // to reject
				// update the status of the marketaction bid and offer with reject(10)
				myMarketActionOffer.getMarketaction().setStatusid(10);
				myMarketActionBid.getMarketaction().setStatusid(10);
				// insert the marketaction in the list to update
				// update the exchangedValue of counteroffer with 0 (to delete)
				marketActionCounterOffer.setExchangedValue(new BigDecimal(0));
				// insert the counteroffer in the list to delete

			} else {
				// update the status of the marketaction bid and offer with accepted(7)
				myMarketActionOffer.getMarketaction().setStatusid(7);
				myMarketActionBid.getMarketaction().setStatusid(7);
				// insert the marketaction in the list to update
				// update the exchangedValue of counteroffer with medium between priceBid and
				// priceOffer (not to delete)
				BigDecimal averageValue = new BigDecimal((priceBid + priceOffer) / 2.0);
				marketActionCounterOffer.setExchangedValue(averageValue);
				// insert the counteroffer in the list to delete
			}

			PrioritisedActionToUpdateList.add(myMarketActionOffer);
			PrioritisedActionToUpdateList.add(myMarketActionBid);
			marketActionCounterOfferToDeleteList.add(marketActionCounterOffer);

		}

		// ----------------------------------------------------------------------------------------------------------------------------------
		// for each MarketAction in input:
		// search id of Action in marketActionToUpdateList
		// if not found
		// insert in marketActionToUpdateList the current action with status reject(10)
		for (Iterator<PrioritisedAction> iterator2 = validActionList.iterator(); iterator2.hasNext();) {
			PrioritisedAction marketAction2 = iterator2.next();
			boolean actionFound = false;
			for (Iterator<PrioritisedAction> iterator3 = PrioritisedActionToUpdateList.iterator(); iterator3
					.hasNext();) {
				PrioritisedAction marketAction3 = iterator3.next();
				if (marketAction3.getMarketaction().getId().equals(marketAction2.getMarketaction().getId())) {
					actionFound = true;
				}
			}
			if (!actionFound) {
				marketAction2.getMarketaction().setStatusid(10); // reject(10)
				PrioritisedActionToUpdateList.add(marketAction2);
			}
		}

		for (Iterator<PrioritisedAction> iterator4 = notValidActionList.iterator(); iterator4.hasNext();) {
			PrioritisedAction marketAction4 = iterator4.next();
			PrioritisedActionToUpdateList.add(marketAction4);
		}

		// --------------------------------------------------------------------------
		returnPayloadMEMO2MCMNotFlex.setClearingPrice(inputPayloadMEMO2MCMNotFlex.getClearingPrice());
		returnPayloadMEMO2MCMNotFlex.setMarketActionCounterOfferList(marketActionCounterOfferToDeleteList);
		returnPayloadMEMO2MCMNotFlex.setPrioritisedActionsList(PrioritisedActionToUpdateList);
		inputPayloadMEMO2MCMNotFlex.getMarketSession().getSessionStatusid().setId(4); // cleared
		returnPayloadMEMO2MCMNotFlex.setMarketSession(inputPayloadMEMO2MCMNotFlex.getMarketSession());

		log.debug("<- clearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

	public void cleanPreviousCounterOffers(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("<- cleanPreviousCounterOffers");

		if (inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList() != null && 
				!inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList().isEmpty()) {

			try {

				URL url = new URL(
						inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() + "/marketactioncounteroffers/");

				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("DELETE");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList());

				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				// test if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//							throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
				//
//						}
				ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				// returnMarketActionCounterOfferList = new Gson().fromJson(br, listType);
				returnMarketActionCounterOfferList = gson.fromJson(br, listType);
				log.debug("payload received: " + gson.toJson(returnMarketActionCounterOfferList));
				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		log.debug("<- cleanPreviousCounterOffers");
		return;
	}

}
