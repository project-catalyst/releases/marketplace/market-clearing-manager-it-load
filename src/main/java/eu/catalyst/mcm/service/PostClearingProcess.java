package eu.catalyst.mcm.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import eu.catalyst.mcm.payload.MarketAction;
import eu.catalyst.mcm.payload.MarketActionCounterOffer;
import eu.catalyst.mcm.payload.PayloadMEMO2MCMNotFlex;
import eu.catalyst.mcm.payload.PrioritisedAction;

@Stateless
@Path("/postClearingProcess/")
public class PostClearingProcess {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(ClearingProcess.class);

	public PostClearingProcess() {
	}

	@POST
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public PayloadMEMO2MCMNotFlex postClearingProcess(PayloadMEMO2MCMNotFlex inputPayloadMEMO2MCMNotFlex) {
		log.debug("-> postClearingProcess");

		PayloadMEMO2MCMNotFlex returnPayloadMEMO2MCMNotFlex = new PayloadMEMO2MCMNotFlex();
		returnPayloadMEMO2MCMNotFlex
				.setInformationBrokerServerUrl(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl());
		returnPayloadMEMO2MCMNotFlex.setTokenMEMO(inputPayloadMEMO2MCMNotFlex.getTokenMEMO());
		returnPayloadMEMO2MCMNotFlex.setTokenMCM(inputPayloadMEMO2MCMNotFlex.getTokenMCM());
		returnPayloadMEMO2MCMNotFlex
				.setMarketClearingManagerUrl(inputPayloadMEMO2MCMNotFlex.getMarketClearingManagerUrl());
		returnPayloadMEMO2MCMNotFlex.setInformationBrokerId(inputPayloadMEMO2MCMNotFlex.getInformationBrokerId());

		// --------------------------------------------------------------------------
		
		if (!inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList().isEmpty()) {

			try {

				List<MarketAction> marketActionList = new ArrayList<MarketAction>();
				
				for (Iterator<PrioritisedAction> iterator = inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList()
						.iterator(); iterator.hasNext();) {
					PrioritisedAction myPrioritisedAction = iterator.next();

					marketActionList.add(myPrioritisedAction.getMarketaction());
					
				}
				
				URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl() 
						+ "/marketplace/"
						+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getMarketplaceid().getId().intValue() 
						+ "/marketsessions/"
						+ inputPayloadMEMO2MCMNotFlex.getMarketSession().getId().intValue() 
						+ "/clearing/actions/");

				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("POST");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(marketActionList);

				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				os.flush();
				os.close();
				connService.disconnect();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// Invoke Rest Service (DELETE) "/marketplace/{marketId}/counteroffers" of IB
				// passing marketActionCounterOfferToDeleteList

		ArrayList<MarketActionCounterOffer> marketActionCounterOfferToDeleteList = new ArrayList<MarketActionCounterOffer>();
		ArrayList<MarketActionCounterOffer> marketActionCounterOfferToBillingList = new ArrayList<MarketActionCounterOffer>();
		ArrayList<MarketActionCounterOffer> returnMarketActionCounterOfferList = new ArrayList<MarketActionCounterOffer>();

		if (!inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList().isEmpty()) {

			for (Iterator<MarketActionCounterOffer> iterator = inputPayloadMEMO2MCMNotFlex.getMarketActionCounterOfferList()
					.iterator(); iterator.hasNext();) {
				MarketActionCounterOffer marketActionCounterOffer = iterator.next();
				if (marketActionCounterOffer.getExchangedValue().doubleValue() == 0.0) {
					marketActionCounterOfferToDeleteList.add(marketActionCounterOffer);
				} else {
					marketActionCounterOfferToBillingList.add(marketActionCounterOffer);
				}
			}

			try {


				URL url = new URL(inputPayloadMEMO2MCMNotFlex.getInformationBrokerServerUrl()  + "/marketactioncounteroffers/");

				// for test URL url = new URL(informationBrokerServerUrl + "/marketplace/" +
				// marketId + "/marketsessions/" + sessionId + "/actions/counteroffers");

				log.debug("Rest url: " + url.toString());

				HttpURLConnection connService = (HttpURLConnection) url.openConnection();
				connService.setDoOutput(true);
				connService.setRequestMethod("DELETE");
				connService.setRequestProperty("Accept", "application/json");
				connService.setRequestProperty("Content-Type", "application/json");
				connService.setRequestProperty("Authorization", inputPayloadMEMO2MCMNotFlex.getTokenMCM());
				GsonBuilder gb = new GsonBuilder();

				// gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				Gson gson = gb.create();

				String record = gson.toJson(marketActionCounterOfferToDeleteList);

				log.debug("Rest Request: " + record);

				OutputStream os = connService.getOutputStream();
				os.write(record.getBytes());

				int myResponseCode = connService.getResponseCode();
				log.debug("HTTP error code :" + myResponseCode);

				// test if (connService.getResponseCode() != HttpURLConnection.HTTP_OK) {
//							throw new RuntimeException("Failed : HTTP error code : " + connService.getResponseCode());
				//
//						}

				BufferedReader br = new BufferedReader(new InputStreamReader((connService.getInputStream())));
				Type listType = new TypeToken<ArrayList<MarketActionCounterOffer>>() {
				}.getType();
				gb = new GsonBuilder();
				gb.setDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
				gson = gb.create();
				// returnMarketActionCounterOfferList = new Gson().fromJson(br, listType);
				returnMarketActionCounterOfferList = gson.fromJson(br, listType);
				log.debug("payload received: " + gson.toJson(returnMarketActionCounterOfferList));
				os.flush();
				os.close();
				br.close();
				connService.disconnect();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

		// --------------------------------------------------------------------------
		returnPayloadMEMO2MCMNotFlex.setClearingPrice(inputPayloadMEMO2MCMNotFlex.getClearingPrice());
		returnPayloadMEMO2MCMNotFlex.setMarketActionCounterOfferList(marketActionCounterOfferToBillingList);
		returnPayloadMEMO2MCMNotFlex.setPrioritisedActionsList(inputPayloadMEMO2MCMNotFlex.getPrioritisedActionsList());
		returnPayloadMEMO2MCMNotFlex.setMarketSession(inputPayloadMEMO2MCMNotFlex.getMarketSession());

		log.debug("<- postClearingProcess");
		return returnPayloadMEMO2MCMNotFlex;
	}

}
