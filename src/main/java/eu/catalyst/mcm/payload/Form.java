package eu.catalyst.mcm.payload;

import java.io.Serializable;

public class Form implements Serializable {
    private static final long serialVersionUID = 1L;
	private Integer id;

	private String form;

	public Form() {
	}

	public Form(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

}
